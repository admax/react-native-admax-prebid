package com.admaxprebid;

import androidx.annotation.NonNull;

import android.os.Bundle;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.google.android.gms.ads.admanager.AdManagerAdRequest;

import org.prebid.mobile.InterstitialAdUnit;
import org.prebid.mobile.OnCompleteListener;
import org.prebid.mobile.PrebidMobile;
import org.prebid.mobile.ResultCode;
import org.prebid.mobile.TargetingParams;

import java.util.Map;

@ReactModule(name = AdmaxPrebidModule.NAME)
public class AdmaxPrebidModule extends ReactContextBaseJavaModule {
  public static final String NAME = "AdmaxPrebid";

  private InterstitialAdUnit interstitialAdUnit;

  public AdmaxPrebidModule(ReactApplicationContext reactContext) {
    super(reactContext);
  }

  @Override
  @NonNull
  public String getName() {
    return NAME;
  }

  @ReactMethod
  public void setLoggingEnabled(boolean loggingEnabled) {
      PrebidMobile.setLoggingEnabled(loggingEnabled);
  }

  @ReactMethod
  public void setPrebidServerAccountId(String accountId) {
      PrebidMobile.setPrebidServerAccountId(accountId);
  }

  @ReactMethod
  public void setShareGeoLocation(boolean shareGeoLocation) {
      PrebidMobile.setShareGeoLocation(shareGeoLocation);
  }

  @ReactMethod
  public void setApplicationContext() {
      PrebidMobile.setApplicationContext(getReactApplicationContext());
  }

  @ReactMethod
  public void setTargetingDomain(String domain) {
      TargetingParams.setDomain(domain);
  }

  @ReactMethod
  public void setTargetingStoreUrl(String storeUrl) {
      TargetingParams.setStoreUrl(storeUrl);
  }

  @ReactMethod
  public void setSubjectToGDPR(boolean subjectToGDPR) {
      TargetingParams.setSubjectToGDPR(subjectToGDPR);
  }

  @ReactMethod
  public void setGDPRConsentString(String consentString) {
      TargetingParams.setGDPRConsentString(consentString);
  }

  @ReactMethod
  public void setPurposeConsents(String purposeConsents) {
      TargetingParams.setPurposeConsents(purposeConsents);
  }

  @ReactMethod
  public void setFakeConsent() {
    this.setSubjectToGDPR(true);
    this.setGDPRConsentString(
      "CPLLDIXPLLDKBAHABAFRBnCsAP_AAH_AAAAAILtf_X__b3_j-_59f_t0eY1P9_7_v-0zjhfdt-8N2f_X_L8X42M7vF36pq4KuR4Eu3LBIQdlHOHcTUmw6okVrzPsbk2Mr7NKJ7PEmnMbO2dYGH9_n93TuZKY7__8___z__-v_v____f_r-3_3__5_X---_e_V399zLv9____39nN___9uCCYBJhqX0AXYljgybRpVCiBGFYSHQCgAooBhaJrCBlcFOyuAj1BCwAQmoCMCIEGIKMWAQACAQBIREBIAeCARAEQCAAEAKkBCAAjYBBYAWBgEAAoBoWIEUAQgSEGRwVHKYEBEi0UE9lYAlF3saYQhllgBQKP6KjARKEECwMhIWDmOAJAS4AA"
    );
    this.setPurposeConsents("1");
  }

  @ReactMethod
  public void initAdmaxConfig() {
      this.setApplicationContext();
      PrebidMobile.initAdmaxConfig(this.getCurrentActivity().getApplication());
  }

  @ReactMethod
  public void loadAd() {
      if (interstitialAdUnit != null) {
          interstitialAdUnit.loadAd();
      }
  }

  @ReactMethod
  public void setIsGoogleAdServerAd(boolean isGoogleAdServerAd) {
      if (interstitialAdUnit != null) {
          interstitialAdUnit.setGoogleAdServerAd(isGoogleAdServerAd);
      }
  }

  @ReactMethod
  public void isAdServerSdkRendering(Promise promise) {
      if (interstitialAdUnit != null) {
          promise.resolve(interstitialAdUnit.isAdServerSdkRendering());
      } else {
          promise.resolve(true);
      }
  }

  @ReactMethod
  public void fetchInterstitialDemand(String configId, Promise promise) {
      interstitialAdUnit = new InterstitialAdUnit(configId);
      AdManagerAdRequest.Builder builder = new AdManagerAdRequest.Builder();
      final AdManagerAdRequest request = builder.build();
      interstitialAdUnit.fetchDemand(request, new OnCompleteListener() {
          @Override
          public void onComplete(ResultCode resultCode) {
              WritableArray resultArray = Arguments.createArray();
              resultArray.pushNull();
              resultArray.pushInt(resultCode.ordinal()); // Assuming ResultCode is an enum
              WritableMap customTargetingMap = Arguments.createMap();
              Bundle customTargetingBundle = request.getCustomTargeting();
              for (String key : customTargetingBundle.keySet()) {
                  String value = customTargetingBundle.getString(key);
                  customTargetingMap.putString(key, value);
              }
              resultArray.pushMap(customTargetingMap);
              promise.resolve(resultArray);
          }
      });
  }
}
