#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(AdmaxPrebid, NSObject)

RCT_EXTERN_METHOD(setLoggingEnabled:(BOOL)loggingEnabled)
RCT_EXTERN_METHOD(setPrebidServerAccountId:(NSString *)accountId)
RCT_EXTERN_METHOD(setShareGeoLocation:(BOOL)shareGeoLocation)
RCT_EXTERN_METHOD(setTargetingDomain:(NSString *)domain)
RCT_EXTERN_METHOD(setTargetingStoreUrl:(NSString *)storeUrl)
RCT_EXTERN_METHOD(setFakeConsent)
RCT_EXTERN_METHOD(initAdmaxConfig)
RCT_EXTERN_METHOD(createInterstitialAdUnit:(NSString *)configId resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
RCT_EXTERN_METHOD(loadAd)
RCT_EXTERN_METHOD(setIsGoogleAdServerAd:(BOOL *)isGoogleAdServerAd)
RCT_EXTERN_METHOD(isAdServerSdkRendering:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
RCT_EXTERN_METHOD(fetchDemand:(AdUnit *)adUnit completion:(RCTResponseSenderBlock)completion)
RCT_EXTERN_METHOD(fetchInterstitialDemand:(NSString *)configId  completion:(RCTResponseSenderBlock)completion)

+ (BOOL)requiresMainQueueSetup
{
  return YES;
}

@end
