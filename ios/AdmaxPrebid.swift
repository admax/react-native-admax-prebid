import Foundation
import AdmaxPrebidMobile
import GoogleMobileAds

@objc(AdmaxPrebid)

class AdmaxPrebid: NSObject, RCTBridgeModule {
  
  var interstitialAdUnit: InterstitialAdUnit?
  
  static func moduleName() -> String! {
    return "AdmaxPrebid"
  }
  
  static var rootViewController: UIViewController? {
      return UIApplication.shared.keyWindow?.rootViewController
  }
  
  @objc
  func setLoggingEnabled(_ loggingEnabled: Bool) {
    DispatchQueue.main.async {
      Prebid.shared.loggingEnabled = loggingEnabled
    }
  }
  
  @objc
  func setPrebidServerAccountId(_ accountId: String) {
    DispatchQueue.main.async {
      Prebid.shared.prebidServerAccountId = accountId
    }
  }
  
  @objc
  func setShareGeoLocation(_ shareGeoLocation: Bool) {
    DispatchQueue.main.async {
      Prebid.shared.shareGeoLocation = shareGeoLocation
    }
  }
  
  @objc
  func setTargetingDomain(_ domain: String) {
    Targeting.shared.domain = domain
  }
  
  @objc
  func setTargetingStoreUrl(_ storeUrl: String) {
    Targeting.shared.storeURL = storeUrl
  }

  @objc 
  func setFakeConsent() {
    UserDefaults.standard.set("1", forKey: "IABTCF_gdprApplies")
    UserDefaults.standard.set("1", forKey: "IABTCF_PurposeConsents")
    UserDefaults.standard.set("CPBQH6JPBQH78AHABAFRA0CsAP_AAH_AAAAAGYtf_X9fb2vj-_5999t0eY1f9_63v-wzjgeNs-8NyZ_X_L4Xr2MyvB34pq4KmR4Eu3LBAQdlHGHcTQmQwIkVqTLsak2Mq7NKJ7JEilMbM2dYGG1vn8XTuZCY70_sf__z_3-_-___67YGXkEmGpfAQJCWMBJNmlUKIEIVxIVAOACihGFo0sNCRwU7K4CPUACABAYgIwIgQYgoxZBAAAAAElEQAkAwIBEARAIAAQArQEIACJAEFgBIGAQACoGhYARRBKBIQZHBUcogQFSLRQTzRgAA", forKey: "IABConsent_ConsentString")
    Targeting.shared.subjectToGDPR = true
  }
  
  @objc
  func initAdmaxConfig() {
    DispatchQueue.main.async {
      Prebid.shared.initAdmaxConfig()
    }
  }
  
  @objc public func loadAd() {
    DispatchQueue.main.async {
      if let adUnit = self.interstitialAdUnit {
        adUnit.loadAd()
      }
    }
  }
  
  @objc public func setIsGoogleAdServerAd(_ isGoogleAdServerAd: Bool) -> Void {
    if let adUnit = self.interstitialAdUnit {
      adUnit.isGoogleAdServerAd = isGoogleAdServerAd
    }
  }
  
  @objc public func isAdServerSdkRendering(_ resolver: @escaping RCTPromiseResolveBlock, rejecter: @escaping RCTPromiseRejectBlock) {
    if let adUnit = self.interstitialAdUnit {
      resolver(adUnit.isAdServerSdkRendering())
    } else {
      resolver(true)
    }
  }
  
  @objc public func fetchInterstitialDemand(_ configId: String, completion: @escaping ([Any]) -> Void) {
      DispatchQueue.main.async {
        if let viewController = AdmaxPrebid.rootViewController {
          self.interstitialAdUnit = InterstitialAdUnit(configId: configId, viewController: viewController)
          let request = GAMRequest()
          if let adUnit = self.interstitialAdUnit {
            adUnit.fetchDemand(adObject: request, completion: { resultCode in
              completion([NSNull(), [resultCode.rawValue, request.customTargeting as Any]])
            })
          }
        } else {
          return
        }
      }
  }
  
  // @objc
  // static func requiresMainQueueSetup() -> Bool {
  //   return true
  // }
}