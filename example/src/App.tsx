import React from 'react';
import { Platform } from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import mobileAds from 'react-native-google-mobile-ads';
import { check, request, PERMISSIONS, RESULTS } from 'react-native-permissions';
import HomeScreen from './screens/HomeScreen';
import AdScreen from './screens/AdScreen';

import { ACCOUNT_ID_ADMAX } from './Constants';
import {
  setLoggingEnabled,
  setApplicationContext,
  setPrebidServerAccountId,
  setShareGeoLocation,
  setFakeConsent,
  initAdmaxConfig,
} from '../../src/index';

const AppNavigator = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    Ad: { screen: AdScreen },
  },
  {
    initialRouteName: 'Home',
  }
);

const AppContainer = createAppContainer(AppNavigator);

const App: React.FC = () => {
  async function initializePermissions() {
    if (Platform.OS === 'ios') {
      const result = await check(PERMISSIONS.IOS.APP_TRACKING_TRANSPARENCY);
      if (result === RESULTS.DENIED) {
        // The permission has not been requested, so request it.
        await request(PERMISSIONS.IOS.APP_TRACKING_TRANSPARENCY);
      }
    }
  }
  initializePermissions();
  mobileAds().initialize();
  setLoggingEnabled(true);
  if (Platform.OS === 'android') {
    setApplicationContext();
  }
  setPrebidServerAccountId(ACCOUNT_ID_ADMAX);
  setShareGeoLocation(true);
  // DO NOT RUN THE FOLLOWING LINE IN PRODUCTION
  // In production, no need to set a fake consent as it is already set by the CMP
  setFakeConsent();
  initAdmaxConfig();
  return <AppContainer />;
};

export default App;
