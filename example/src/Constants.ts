export const ACCOUNT_ID_ADMAX = '4803423e-c677-4993-807f-6a1554477ced';
export const CONFIG_ID_ALL_XANDR = 'dbe12cc3-b986-4b92-8ddb-221b0eb302ef';
export const CONFIG_ID_INTERSTITIAL_SMART =
  '2cd143f6-bb9d-4ca9-9c4b-acb527657177';
export const CONFIG_ID_320x50_SMART = 'fe7d0514-530c-4fb3-9a52-c91e7c426ba6';
export const CONFIG_ID_INTERSTITIAL_CRITEO =
  '5ba30daf-85c5-471c-93b5-5637f3035149';
export const CONFIG_ID_320x50_CRITEO = 'fb5fac4a-1910-4d3e-8a93-7bdbf6144312';
export const CONFIG_ID_300x250_CRITEO = '5ba30daf-85c5-471c-93b5-5637f3035149';
export const GAM_BANNER_ADUNIT_ID_320x50_ADMAX =
  '/21807464892/pb_admax_320x50_top';
export const GAM_BANNER_ADUNIT_ID_300x250_ADMAX =
  '/21807464892/pb_admax_300x250_top';
export const GAM_INTERSTITIAL_ADUNIT_ID_ADMAX =
  '/21807464892/pb_admax_interstitial';
export const APP_EVENT_MAX_DELAY = 30;
