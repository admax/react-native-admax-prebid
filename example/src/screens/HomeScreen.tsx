import React, {useState} from 'react';
import {View, Button, StyleSheet, ScrollView} from 'react-native';
import {Picker} from '@react-native-community/picker';
import {NavigationScreenProp} from 'react-navigation';

interface Props {
  navigation: NavigationScreenProp<any, any>;
}

const HomeScreen: React.FC<Props> = ({navigation}) => {
  const [selectedAdFormat, setSelectedAdFormat] = useState('interstitial');
  const [selectedAdSize, setSelectedAdSize] = useState('320x50');
  const [selectedBidder, setSelectedBidder] = useState('Xandr');

  const goToAdScreen = () => {
    navigation.navigate('Ad', {
      adFormat: selectedAdFormat,
      adSize: selectedAdSize,
      bidder: selectedBidder,
    });
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Picker
        selectedValue={selectedAdFormat}
        onValueChange={(itemValue: any) => setSelectedAdFormat(itemValue)}
        style={styles.picker}>
        <Picker.Item label="Banner" value="banner" />
        <Picker.Item label="Interstitial" value="interstitial" />
      </Picker>
      <Picker
        selectedValue={selectedAdSize}
        onValueChange={(itemValue: any) => setSelectedAdSize(itemValue)}
        style={styles.picker}>
        <Picker.Item label="320x50" value="320x50" />
        <Picker.Item label="300x250" value="300x250" />
      </Picker>
      <Picker
        selectedValue={selectedBidder}
        onValueChange={(itemValue: any) => setSelectedBidder(itemValue)}
        style={styles.picker}>
        <Picker.Item label="Smart" value="Smart" />
        <Picker.Item label="Criteo" value="Criteo" />
        <Picker.Item label="Xandr" value="Xandr" />
      </Picker>
      <View style={styles.buttonContainer}>
        <Button title="Show Ad" onPress={goToAdScreen} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    padding: 16,
  },
  picker: {
    marginBottom: 12,
  },
  buttonContainer: {
    alignItems: 'center',
  },
});

export default HomeScreen;
