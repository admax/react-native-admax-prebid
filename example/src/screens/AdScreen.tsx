import React, { useEffect, useRef, useState } from 'react';
import { View, Platform } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import {
  GAMInterstitialAd,
  AdEventType,
  GAMBannerAd,
  BannerAdSize,
  GAMAdEventType,
  AppEvent,
} from 'react-native-google-mobile-ads';
import {
  APP_EVENT_MAX_DELAY,
  CONFIG_ID_ALL_XANDR,
  CONFIG_ID_INTERSTITIAL_CRITEO,
  CONFIG_ID_INTERSTITIAL_SMART,
  GAM_BANNER_ADUNIT_ID_300x250_ADMAX,
  GAM_BANNER_ADUNIT_ID_320x50_ADMAX,
  GAM_INTERSTITIAL_ADUNIT_ID_ADMAX,
} from '../Constants';
import {
  setIsGoogleAdServerAd,
  isAdServerSdkRendering,
  loadAd,
  fetchInterstitialDemand,
} from '../../../src/index';

interface Props {
  navigation: NavigationScreenProp<any, any>;
}

const AdScreen: React.FC<Props> = ({ navigation }) => {
  const [interstitial, setInterstitial] = useState<GAMInterstitialAd | null>(
    null
  );
  const { adFormat, adSize, bidder } = navigation.state.params;

  if (adFormat === 'interstitial') {
    const [loaded, setLoaded] = useState(false);
    const timerIdRef = useRef<NodeJS.Timeout | null>(null);
    const admaxConfigId =
      bidder === 'Xandr'
        ? CONFIG_ID_ALL_XANDR
        : bidder === 'Smart'
        ? CONFIG_ID_INTERSTITIAL_SMART
        : CONFIG_ID_INTERSTITIAL_CRITEO;

    useEffect(() => {
      const onDemandFetched = (...args: any) => {
        let error, data, resultCode, customTargeting;
        if (Platform.OS === 'ios') {
          [error, data] = args;
          [resultCode, customTargeting] = data;
        } else {
          [data] = args;
          [error, resultCode, customTargeting] = data;
        }
        if (error) {
          console.error(error);
          return;
        }
        console.log('Fetch Demand result code:', resultCode);
        console.log('Custom targeting:', customTargeting);

        const adUnitId = GAM_INTERSTITIAL_ADUNIT_ID_ADMAX;
        const adUnit = GAMInterstitialAd.createForAdRequest(adUnitId, {
          customTargeting,
        });

        setInterstitial(adUnit);
        adUnit.addAdEventListener(AdEventType.LOADED, () => {
          setLoaded(true);
          timerIdRef.current = setTimeout(() => {
            console.log('Showing GAM Ad');
            adUnit.show();
          }, APP_EVENT_MAX_DELAY);
        });
        adUnit.addAdEventListener(
          GAMAdEventType.APP_EVENT,
          (payload: AppEvent) => {
            if (payload.name === 'bidWon') {
              if (timerIdRef.current) {
                clearTimeout(timerIdRef.current);
              }
              setIsGoogleAdServerAd(false);
              isAdServerSdkRendering().then(
                (isAdServerSdkRendering: boolean) => {
                  if (!isAdServerSdkRendering) {
                    console.log('Client side rendering');
                    loadAd();
                  } else {
                    console.log('Server side rendering');
                    adUnit.show();
                  }
                }
              );
            }
          }
        );
        adUnit.load();
      };

      fetchInterstitialDemand(admaxConfigId, onDemandFetched);
    }, []);

    // No advert ready to show yet
    if (!loaded) {
      return null;
    }

    return <View></View>;
  }

  return (
    <View>
      <GAMBannerAd
        sizes={
          adSize === '320x50'
            ? [BannerAdSize.BANNER]
            : [BannerAdSize.MEDIUM_RECTANGLE]
        }
        unitId={
          adSize === '320x50'
            ? GAM_BANNER_ADUNIT_ID_320x50_ADMAX
            : GAM_BANNER_ADUNIT_ID_300x250_ADMAX
        }
      />
    </View>
  );
};

export default AdScreen;
