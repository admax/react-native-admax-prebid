import { NativeModules, Platform } from 'react-native';

const LINKING_ERROR =
  `The package 'react-native-admax-prebid' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo Go\n';

const AdmaxPrebid = NativeModules.AdmaxPrebid
  ? NativeModules.AdmaxPrebid
  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

export function setLoggingEnabled(loggingEnabled: boolean): void {
  AdmaxPrebid.setLoggingEnabled(loggingEnabled);
}

export function setPrebidServerAccountId(accountId: string): void {
  AdmaxPrebid.setPrebidServerAccountId(accountId);
}

export function setShareGeoLocation(shareGeoLocation: boolean): void {
  AdmaxPrebid.setShareGeoLocation(shareGeoLocation);
}

export function setApplicationContext(): void {
  AdmaxPrebid.setApplicationContext();
}

export function setTargetingDomain(domain: string): void {
  AdmaxPrebid.setTargetingDomain(domain);
}

export function setTargetingStoreUrl(storeUrl: string): void {
  AdmaxPrebid.setTargetingStoreUrl(storeUrl);
}

export function setSubjectToGDPR(subjectToGDPR: boolean): void {
  AdmaxPrebid.setSubjectToGDPR(subjectToGDPR);
}

export function setGDPRConsentString(consentString: string): void {
  AdmaxPrebid.setGDPRConsentString(consentString);
}

export function setPurposeConsents(purposeConsents: string): void {
  AdmaxPrebid.setPurposeConsents(purposeConsents);
}

export function setFakeConsent(): void {
  AdmaxPrebid.setFakeConsent();
}

export function initAdmaxConfig(): void {
  AdmaxPrebid.initAdmaxConfig();
}

export function loadAd(): void {
  AdmaxPrebid.loadAd();
}

export function setIsGoogleAdServerAd(isGoogleAdServerAd: boolean): void {
  AdmaxPrebid.setIsGoogleAdServerAd(isGoogleAdServerAd);
}

export function isAdServerSdkRendering(): Promise<boolean> {
  return AdmaxPrebid.isAdServerSdkRendering();
}

export function fetchInterstitialDemand(
  configId: string,
  onDemandFetched: (...args: any) => void
): void {
  if (Platform.OS === 'ios') {
    AdmaxPrebid.fetchInterstitialDemand(configId, onDemandFetched);
  } else {
    AdmaxPrebid.fetchInterstitialDemand(configId).then(onDemandFetched);
  }
}
