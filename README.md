# react-native-admax-prebid

Admax React Native Prebid Mobile Adapter

## Installation

```sh
npm install react-native-admax-prebid
```

## Usage

First initialize the SDK

```js
import {
  setLoggingEnabled,
  setApplicationContext,
  setPrebidServerAccountId,
  setShareGeoLocation,
  setFakeConsent,
  initAdmaxConfig,
} from 'react-native-admax-prebid';

// ...

const App: React.FC = () => {
  async function initializePermissions() {
    if (Platform.OS === 'ios') {
      const result = await check(PERMISSIONS.IOS.APP_TRACKING_TRANSPARENCY);
      if (result === RESULTS.DENIED) {
        // The permission has not been requested, so request it.
        await request(PERMISSIONS.IOS.APP_TRACKING_TRANSPARENCY);
      }
    }
  }
  initializePermissions();
  mobileAds().initialize();
  setLoggingEnabled(true);
  if (Platform.OS === 'android') {
    setApplicationContext();
  }
  setPrebidServerAccountId(ACCOUNT_ID_ADMAX);
  setShareGeoLocation(true);
  // DO NOT RUN THE FOLLOWING LINE IN PRODUCTION
  // In production, no need to set a fake consent as it is already set by the CMP
  setFakeConsent();
  initAdmaxConfig();
  return <AppContainer />;
};
```

Then fetch an interstitial

```js
import {
  setIsGoogleAdServerAd,
  isAdServerSdkRendering,
  loadAd,
  fetchInterstitialDemand,
} from 'react-native-admax-prebid';

// ...

// In a useEffect() for instance:
const onDemandFetched = (...args: any) => {
  let error, data, resultCode, customTargeting;
  if (Platform.OS === 'ios') {
    [error, data] = args;
    [resultCode, customTargeting] = data;
  } else {
    [data] = args;
    [error, resultCode, customTargeting] = data;
  }
  if (error) {
    console.error(error);
    return;
  }
  console.log('Fetch Demand result code:', resultCode);
  console.log('Custom targeting:', customTargeting);

  const adUnitId = GAM_INTERSTITIAL_ADUNIT_ID_ADMAX;
  const adUnit = GAMInterstitialAd.createForAdRequest(adUnitId, {
    customTargeting,
  });

  setInterstitial(adUnit);
  adUnit.addAdEventListener(AdEventType.LOADED, () => {
    setLoaded(true);
    timerIdRef.current = setTimeout(() => {
      console.log('Showing GAM Ad');
      adUnit.show();
    }, APP_EVENT_MAX_DELAY);
  });
  adUnit.addAdEventListener(GAMAdEventType.APP_EVENT, (payload: AppEvent) => {
    if (payload.name === 'bidWon') {
      if (timerIdRef.current) {
        clearTimeout(timerIdRef.current);
      }
      setIsGoogleAdServerAd(false);
      isAdServerSdkRendering().then((isAdServerSdkRendering: boolean) => {
        if (!isAdServerSdkRendering) {
          console.log('Client side rendering');
          loadAd();
        } else {
          console.log('Server side rendering');
          adUnit.show();
        }
      });
    }
  });
  adUnit.load();
};

fetchInterstitialDemand(admaxConfigId, onDemandFetched);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
